<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
 
class ArticleController extends Controller
{
    public function index()
    {
        return response()->json([
            'status code'=>200,
            'message'=>'list article',
            'articles'=>Article::all()
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $field=$request->validate([

            'title'=>'required',
            'body'=>'required'
        ]);
        $article =Article ::create($field);
         return response()->json([
             'status code'=>201,
            'message'=>"create article success",
            'article'=>$article
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article=Article::find($id);
        if($article){
        return response()->json([
            'status code'=>200,
            'message'=>'show artilce '.$id.' success',
            'article'=>$article]
            );
        }
        return response()->json([
             'status code'=>404,
            'message'=>'Not found']
             );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article =Article::find($id);
        if($article){
        $field=$request->validate([
            'title'=>'required',
            'body'=>'required'
        ]);
        $article->update($field);
        return response()->json([
            'status code'=>200,
            'message'=>'edit article '.$id.' success',
            'article'=>$article]
            );
        } return response()->json([
            'status code'=>404,
            'message'=>'Not found']
            );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $article=Article::find($id);
       if($article){
        $article->delete();
        return response()->json([
            'status code'=>204,
            'message'=>'delete article '.$id.' success'],
             );
       }
       return response()->json([
            'status code'=>404,
            'message'=>'Not found']
            );
    }


}
